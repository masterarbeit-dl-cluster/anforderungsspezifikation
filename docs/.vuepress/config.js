const glob = require('glob')

let funktionaleAnf = glob.sync('docs/Funktionale-Anforderungen/*.md').map(s => s.split('/').pop()).filter(s => s !== 'README.md')
let nichtFunktionaleAnf = glob.sync('docs/Nicht-Funktionale-Anforderungen/*.md').map(s => s.split('/').pop()).filter(s => s !== 'README.md')

module.exports = {
    title: 'DL-Cluster Anforderungsspezifikation',
    description: 'Just playing around',
    themeConfig: {
        nav: [
            { text: 'Einführung', link: '/Einführung/' },
            { text: 'Beschreibung', link: '/Beschreibung/Produkteinordnung' },
            {
                text: 'Anforderungen', items: [
                    { text: 'Funktionale Anforderungen', link: '/Funktionale-Anforderungen/' },
                    { text: 'Nicht-Funktionale Anforderungen', link: '/Nicht-Funktionale-Anforderungen/' },
                ]
            },
          { text: 'Glossar', link: '/Glossar/'},
          { text: 'Dokumentation', link: '/Dokumentation/'}  
        ],
        sidebar: {
            '/Einführung/': getEinfuehrungSidebar(),
            '/Beschreibung/': getBeschreibungSidebar(),
            '/Funktionale-Anforderungen/': getFunktionaleAnfSidebar(),
            '/Nicht-Funktionale-Anforderungen/': getNichtFunktionaleAnfSidebar(),
        },
    }
}

function getEinfuehrungSidebar() {
    return [{
        title: 'Einführung',
        // children: [
        //     'Zweck.md',
        //     'Abgrenzung',
        //     'Überblick'
        // ]
    }]
}

function getBeschreibungSidebar() {
    return [{
        title: 'Beschreibung',
        children: [
            'Produkteinordnung',
            'Produktfunktionen',
            'Stakeholder',
            'Benutzereigenschaften'
        ]
    }]
}

function getFunktionaleAnfSidebar() {
    return [{
        title: 'Funktionale Anforderungen',
        children: [''].concat(funktionaleAnf),
    }]
}

function getNichtFunktionaleAnfSidebar() {
    return [{
        title: 'Nicht-Funktionale Anforderungen',
        children: [''].concat(nichtFunktionaleAnf),
    }]
}