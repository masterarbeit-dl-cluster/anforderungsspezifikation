# Wunschanforderungen
* Das Deep Learning-Clustersystem soll dem Anwender die Möglichkeit bieten, Datensätze aus externen Ressourcen hochzuladen.
* Das Deep Learning-Clustersystem soll dem Anwender die Möglichkeit bieten, den Quellcode eines Deep Learning-Lernprozesses per GIT-Repository hochladen zu können. (hochladen - Vollverb?)
* Das Deep Learning-Clustersystem soll dem Administrator die Möglichkeit bieten, Deep Learning-Lernprozesse für alle Anwender frei zu priorisieren.
* Das Deep Learning-Clustersystem soll dem Anwender die Möglichkeit bieten, Hyperparameter zu tunen.
* Das Deep Learning-Clustersystem soll dem Anwender die Möglichkeit bieten, trainierte Modelle über eine Schnittstelle freizugeben. (z.B. TFServing)
* Das Deep Learning-Clustersystem soll dem Anwender die Möglichkeit bieten, außerhalb des Hochschulnetzes auf den Cluster zuzugreifen. (-> Internet)