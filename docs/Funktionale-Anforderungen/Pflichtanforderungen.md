# Pflichtanforderungen
## Statusvisualisierung/Beobachtung Trainingsprozess
* Das Deep Learning-Clustersystem muss dem Anwender die Möglichkeit bieten, den Status eines laufenden Deep Learning-Lernprozesses zu beobachten. (visualisieren zu ungenau; das WIE (HTML etc.) ist aber nicht wichtig!) (selbstst. Systemaktivität oder Benutzerinteraktion??) (den Status eines seiner Prozesse, oder ALLER?) (Hierarchie Admin, Mitarbeiter, Student) (TensorBoard - lösungsneutral???, ist aber eig auch konkrete Anforderung -> S. 233)
* Falls während eines Deep Learning-Lernprozesses ein Fehler auftritt, muss das Deep Learning-Clustersystem den Anwender informieren. (zu ungenau - kein Vollverb; EIN Fehler - Mengenwort, Fehlerart evtl. angeben)
  
## Datenhaltung
??? Wann werden Daten gelöscht? Nach Zeitspanne? Nach erfolgreichem Training? Nie?

* Das Deep Learning-Clustersystem muss die Trainingsdaten eines Deep Learning-Lernprozesses extern speichern. (was bedeutet extern -> mehrmaliger Zugriff, nicht in einen Container mit dem Rest -> Glossar, oder genauer beschreiben)
* Das Deep Learning-Clustersystem muss dem Anwender die Möglichkeit bieten, lokale Trainingsdaten hochzuladen.
* Das Deep Learning-Clustersystem muss die Trainingsdaten eines Lernvorgangs über mehrere Teilschritte persistent halten. 
* Das Deep Learning-Clustersystem muss dem Anwender die Möglichkeit bieten, auf seine hochgeladenen Trainingsdaten eines Deep Learning-Lernprozesses per Schnittstelle zuzugreifen. (zugreifen - Vollverb?; z.B. Minio - Daten Persistent über mehrere Teilschritte, REST API)
* Falls im Quellcode eines Deep Learning-Lernprozesses Checkpoints vorhanden sind, muss das Deep Learning-Clustersystem dem Anwender die Möglichkeit bieten, das Training zu einem späteren Zeitpunkt ohne Datenverlust weiterzuführen.
* Das Deep Learning-Clustersystem muss große Datensätze effizient verwalten. (groß, effizient -> klären; ist das FA oder NFA?)

## Lernart/Zugriff
* Das Deep Learning-Clustersystem muss dem Anwender die Möglichkeit bieten, den Quellcode eines Deep Learning-Lernprozesses per Jupyter Notebook hochladen zu können.
* Das Deep Learning-Clustersystem muss dem Anwender die Möglichkeit bieten, den Quellcode eines Deep Learning-Lernprozesses per Docker-Container hochladen zu können.
* Das Deep Learning-Clustersystem muss dem Anwender die Möglichkeit bieten, im Quellcode eines Deep Learning-Lernprozesses gängige Deep Learning-Bibliotheken zu verwenden. (defininere dabei gängig)

## Priorisierung/Einschränkungen
* Das Deep Learning-Clustersystem muss dem Mitarbeiter die Möglichkeit bieten, temporär alle verfügbaren Knoten für einen Deep Learning-Lernprozess zu blockieren.
* Das Deep Learning-Clustersystem muss die Arbeitsbereiche aller Anwender voneinander abgrenzen.
* Das Deep Learning-Clustersystem muss dem Mitarbeiter die Möglichkeit bieten, für die ihm zugewiesene Studenten Quotas festzulegen.
* Das Deep Learning-Clustersystem muss dem Mitarbeiter die Möglichkeit bieten, die Quotas der ihm zugewiesene Studenten nachträglich zu verändern.
