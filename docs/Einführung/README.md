---
sidebar: auto
---

# Einführung

## Zweck des Dokuments

Der Zweck dieses Dokuments ist es, einen Überblick über das Deep Learning-Clustersystems des Adrive Living Lab und die damit verbundene Software zu geben. Es enthält funktionale und nicht-funktionale Anforderungen, eine Beschreibung des Systems, der damit verbundenen Begriffe sowie aller beteiligten Stakeholder. 

## Konvention
Die Gliederung dieser Anforderungsspezifikation basiert lose auf der [Software Requirements Specification der IEEE-Norm 29148:2018](https://de.wikipedia.org/wiki/Software_Requirements_Specification).

## Zielgruppe
Die Hauptzielgruppe dieses Dokuments stellen vor allem Endanwender dar, welche Deep Learning-Lernvorgänge auf dem Cluster erledigen wollen. Darüberhinaus gibt die Anfoderungsspezifikation einen guten Ausgangspunkt für Entwickler, welche dieses Produkt erweitern möchten.

## Definitionen
Definitionen und Abgrenzungen einzelner Begriffe finden sich im [Glossar](/Glossar) wieder.

## Referenzen
Eine Dokumentation für die Endanwender findet sich unter [Dokumentation](/Dokumentation).